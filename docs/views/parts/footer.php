<section class="section k_bg-light">
  <header class="k_section__header">Footer</header>
</section>

<footer class="footer section" role="contentinfo">
  <a class="__logo" href="http://www.northeastern.edu" aria-label="Northeastern University logo">Northeastern University</a>
  <div class="__body">
    <ul class="__list">
      <li><a target="_blank" href="https://my.northeastern.edu/welcome">myNortheastern</a></li>
      <li><a target="_blank" href="https://prod-web.neu.edu/webapp6/employeelookup/public/main.action">Find Faculty &amp; Staff</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/neuhome/adminlinks/findaz.html">Find A-Z</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/emergency/index.html">Emergency Information</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/search/index.html">Search</a></li>
      <li><a target="_blank" href="http://www.northeastern.edu/privacy/index.html">Privacy</a></li>
    </ul>
    <div class="__copy">
      360 Huntington Ave., Boston, Massachusetts 02115 &nbsp;&bull;&nbsp; 617.373.2000 &nbsp;&bull;&nbsp; TTY 617.373.3768
      <br>&copy; 2017 Northeastern University
    </div>
  </div>
</footer>
