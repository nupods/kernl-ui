<section class="section">
  <header class="k_section__header">Base</header>
  <div class="row">
    <div class="col w--50@t">
      <p class="text-style-block">Default: thumb on the left</p>
      <div class="media --xs">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">This is a XS media object.</div>
            <p class="__copy">Etiam porta sem malesuada magna mollis euismod. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
          </div>
        </a>
      </div>
      <div class="media --sm --rect">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">This is a SM media object.</div>
            <p class="__copy">Has <code>--rect</code> class on <code>media</code> element. Nullam quis risus eget urna mollis ornare vel eu leo. Cras mattis consectetur purus sit amet fermentum.</p>
          </div>
        </a>
      </div>
      <div class="media">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">This is a basic media object.</div>
            <p class="__copy">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          </div>
        </a>
      </div>
      <div class="media --lg">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">This is a LG media object.</div>
            <p class="__copy">Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col w--50@t">
      <p class="text-style-block">Variation: thumb on the right</p>
      <div class="media --xs --right">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">This is a XS media object.</div>
            <p class="__copy">Etiam porta sem malesuada magna mollis euismod. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
          </div>
        </a>
      </div>
      <div class="media --sm --right">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">This is a SM media object.</div>
            <p class="__copy">Nullam quis risus eget urna mollis ornare vel eu leo. Cras mattis consectetur purus sit amet fermentum.</p>
          </div>
        </a>
      </div>
      <div class="media --right">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">This is a basic media object.</div>
            <p class="__copy">Aenean lacinia bibendum nulla sed consectetur. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          </div>
        </a>
      </div>
      <div class="media --lg --right --rect">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">This is a LG media object.</div>
            <p class="__copy">Has <code>--rect</code> class on <code>media</code> element. Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Donec id elit non mi porta gravida at eget metus.</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>

<!-- media object list -->
<section class="section">
  <header class="k_section__header">Media Object List</header>
  <div class="row">
    <div class="col w--50@t">
      <p class="text-style-block">Default: circular thumb</p>
      <ul class="list-group --media">
        <li class="media">
          <a class="__link" href="#">
            <figure class="__graphic"></figure>
            <div class="__body">
              <div class="__title">This: media object list item.</div>
            <p class="__copy">Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.</p>
            </div>
          </a>
        </li>
        <li class="media">
          <a class="__link" href="#">
            <figure class="__graphic"></figure>
            <div class="__body">
              <div class="__title">Another media object list item.</div>
              <p class="__copy">Etiam porta sem malesuada magna mollis euismod. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
            </div>
          </a>
        </li>
        <li class="media">
          <a class="__link" href="#">
            <figure class="__graphic"></figure>
            <div class="__body">
              <div class="__title">Three shall be the number thou shalt count.</div>
              <p class="__copy">Four shalt thou not count, nor either count thou two, excepting that thou then proceed to three. Five is right out!</p>
            </div>
          </a>
        </li>
        <li class="media">
          <a class="__link" href="#">
            <figure class="__graphic"></figure>
            <div class="__body">
              <div class="__title">Nibh Ornare Parturient Amet</div>
              <p class="__copy">Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras mattis consectetur purus sit amet fermentum.</p>
            </div>
          </a>
        </li>
      </ul>
    </div>
    <div class="col w--50@t">
      <p class="text-style-block">Variation: rectangular thumb</p>
      <ul class="list-group --media --rect">
        <li class="media">
          <a class="__link" href="#">
            <figure class="__graphic"></figure>
            <div class="__body">
              <div class="__title">This: media object list item.</div>
            <p class="__copy">Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper.</p>
            </div>
          </a>
        </li>
        <li class="media">
          <a class="__link" href="#">
            <figure class="__graphic"></figure>
            <div class="__body">
              <div class="__title">Another media object list item.</div>
              <p class="__copy">Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div>
          </a>
        </li>
        <li class="media">
          <a class="__link" href="#">
            <figure class="__graphic"></figure>
            <div class="__body">
              <div class="__title">Three shall be the number thou shalt count.</div>
              <p class="__copy">Four shalt thou not count, nor either count thou two, excepting that thou then proceed to three. Five is right out!</p>
            </div>
          </a>
        </li>
        <li class="media">
          <a class="__link" href="#">
            <figure class="__graphic"></figure>
            <div class="__body">
              <div class="__title">Vehicula Nullam Ipsum Justo</div>
              <p class="__copy">Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</p>
            </div>
          </a>
        </li>
      </ul>
    </div>
  </div>
</section>

<!-- Equal-height Variant -->
<section class="section col--stretch">
  <header class="k_section__header">Equal-height Variant</header>
  <div class="bg--beige pa--1 mb--1 br--pill fs--xs">
    Add <code>col--stretch</code> class to parent <code>section</code>. Makes media objects parent columns within a row equal height. <b>Note</b>: Padding should be applied in the column's custom classes on the Wordpress backend.
  </div>
  <div class="row">
    <div class="col w--50@t w--1/3@d">
      <div class="media">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">Firstname Lastname</div>
            <p class="__copy">Short Job Title</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col w--50@t w--1/3@d">
      <div class="media">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">Dr. Firstname Lastname</div>
            <p class="__copy">Director of Self-Importance, Benevolence and Faux-Philanthopy; Full-Time Adjunct Professor and Research Fellow of Self Interest and Unnecessarily Long Job Titles</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col w--50@t w--1/3@d">
      <div class="media">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">Firstname M. Lastname</div>
            <p class="__copy">Assistant Professor of Something Requiring a Slightly Longer Title Than Others</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col w--50@t w--1/3@d">
      <div class="media">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">Firstname M. Lastname</div>
            <p class="__copy">Assistant Professor of Something Requiring a Slightly Longer Title Than Others</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col w--50@t w--1/3@d">
      <div class="media">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">Firstname Lastname</div>
            <p class="__copy">Short Job Title</p>
          </div>
        </a>
      </div>
    </div>
    <div class="col w--50@t w--1/3@d">
      <div class="media">
        <a class="__link" href="#">
          <figure class="__graphic"></figure>
          <div class="__body">
            <div class="__title">Firstname M. Lastname</div>
            <p class="__copy">Assistant Professor of Something Requiring a Slightly Longer Title Than Others</p>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>
