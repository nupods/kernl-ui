
<header class="section --banner bg--img">
  <div class="__header">
    <h2 class="__title">Example Interior/On-Page Nav</h2>
  </div>
</header>

<section class="section">

  <div class="row">
    <div class="col w--1/2@t w--1/4@d">
      <div class="hidden--up@t ta--c">
        <button class="btn --sm" data-toggle="nav" data-swap-target="#page_menu_0">
          <i data-feather="align-left"></i>
          Page Menu
        </button>
      </div>
      <nav class="nav --interior" id="page_menu_0">
        <ul class="__list">
          <li class="__item --active">
            <a class="__link" href="#">Dolor Fermentum Ullamcorper Fringilla</a>
          </li>
          <li class="__item">
            <a class="__link" href="#">Dolor Fermentum Ullamcorper Fringilla</a>
          </li>
          <li class="__item">
            <a class="__link" href="#">Dolor Fermentum Ullamcorper Fringilla</a>
          </li>
          <li class="__item">
            <a class="__link" href="#">Dolor Fermentum Ullamcorper Fringilla</a>
          </li>
          <li class="__item">
            <a class="__link" href="#">Sit Parturient Pellentesque Magna Sem Ridiculus Ultricies Euismod</a>
          </li>
        </ul>
      </nav>
    </div>
    <div class="col w--1/2@t w--1/4@d">
      <div class="hidden--up@t ta--c">
        <button class="btn --sm" data-toggle="nav" data-swap-target="#page_menu_1">
          <i data-feather="align-left"></i>
          Page Menu
        </button>
      </div>
      <nav class="nav --interior" id="page_menu_1">
        <ul class="__list">
          <li class="__item">
            <a class="__link" href="#">Dolor Fermentum Ullamcorper Fringilla</a>
          </li>
          <li class="__item">
            <a class="__link" href="#">Dolor Fermentum Ullamcorper Fringilla</a>
          </li>
          <li class="__item">
            <a class="__link" href="#">Sit Parturient Pellentesque Magna Sem Ridiculus Ultricies Euismod</a>
          </li>
          <li class="__item +children --active">
            <a class="__link" href="#">Lorem Aenean Sit Sem</a>
            <ul class="__list">
              <li class="__item">
                <a class="__link" href="#">Cras Lorem</a>
              </li>
              <li class="__item">
                <a class="__link" href="#">Ridiculus Purus Ultricies Vehicula Euismod Sit Parturient Pellentesque Magna Sem</a>
              </li>
              <li class="__item">
                <a class="__link" href="#">Tristique Ultricies Ligula Parturient</a>
              </li>
            </ul>
          </li>
        </ul>
      </nav>
    </div>
    <div class="col w--1/2@t w--1/4@d">
      <div class="hidden--up@t ta--c">
        <button class="btn --sm" data-toggle="nav" data-swap-target="#page_menu_2">
          <i data-feather="align-left"></i>
          Page Menu
        </button>
      </div>
      <nav class="nav --interior" id="page_menu_2">
        <ul class="__list">
          <li class="__item --title">
            <a class="__link" href="#">
              <i data-feather="corner-left-up"></i> Parent Page 
            </a> 
          </li>
          <li class="__item">
            <a class="__link" href="#">Dolor Fermentum Ullamcorper Fringilla</a>
          </li>
          <li class="__item --active +children">
            <a class="__link" href="#">Lorem Aenean Sit Sem</a>
            <ul class="__list">
              <li class="__item">
                <a class="__link" href="#">Cras Lorem</a>
              </li>
              <li class="__item --active">
                <a class="__link" href="#">Ridiculus Purus Ultricies Vehicula Euismod Sit Parturient Pellentesque Magna Sem</a>
              </li>
              <li class="__item">
                <a class="__link" href="#">Tristique Ultricies Ligula Parturient</a>
              </li>
            </ul>
          </li>
          <li class="__item">
            <a class="__link" href="#">Sit Parturient Pellentesque Magna Sem Ridiculus Ultricies Euismod</a>
          </li>
        </ul>
      </nav>
    </div>
  <div class="col w--1/2@t w--1/4@d">
    <div class="hidden--up@t ta--c">
      <button class="btn --sm" data-toggle="nav" data-swap-target="#page_menu_3">
        <i data-feather="align-left"></i>
        Page Menu
      </button>
    </div>
    <nav class="nav --interior" id="page_menu_3">
      <ul class="__list">
        <li class="__item --title">
          <a class="__link" href="#">
            <i data-feather="corner-left-up"></i> Parent Page With Wrapping Text for Length
          </a> 
        </li>
        <li class="__item">
          <a class="__link" href="#">Dolor Fermentum Ullamcorper Fringilla</a>
        </li>
        <li class="__item +children">
          <a class="__link" href="#">Lorem Aenean Sit Sem</a>
          <ul class="__list">
            <li class="__item">
              <a class="__link" href="#">Cras Lorem</a>
            </li>
            <li class="__item">
              <a class="__link" href="#">Ridiculus Purus Ultricies Vehicula Euismod Sit Parturient Pellentesque Magna Sem</a>
            </li>
            <li class="__item">
              <a class="__link" href="#">Tristique Ultricies Ligula Parturient</a>
            </li>
          </ul>
        </li>
        <li class="__item --active +children">
          <a class="__link" href="#">Lorem Aenean Sit Sem</a>
          <ul class="__list">
            <li class="__item">
              <a class="__link" href="#">Cras Lorem</a>
            </li>
            <li class="__item --active">
              <a class="__link" href="#">Ridiculus Purus Ultricies Vehicula Euismod Sit Parturient Pellentesque Magna Sem</a>
            </li>
            <li class="__item">
              <a class="__link" href="#">Tristique Ultricies Ligula Parturient</a>
            </li>
          </ul>
        </li>
        <li class="__item">
          <a class="__link" href="#">Sit Parturient Pellentesque Magna Sem Ridiculus Ultricies Euismod</a>
        </li>
      </ul>
    </nav>
  </div>
  </div>
</section>

<section class="section --banner bg--img +nav">
  <div class="__header">
    <h1 class="__title">Banner with Tabbed Navigation</h1>
    <div class="__subtitle">Includes Background Image</div>
  </div>
  <nav class="nav --tabbed">
    <ul class="__list" role="tablist">
      <li class="__item --title">
        <a class="__link --active" data-toggle="tab" href="#tab_1" data-swap-group="tabgroup_1" role="tab" aria-expanded="true">Tab Number One</a>
      </li>
      <li class="__item">
        <a class="__link" data-toggle="tab" href="#tab_2" data-swap-group="tabgroup_1" role="tab" aria-expanded="false">The Second Tab</a>
      </li>
      <li class="__item">
        <a class="__link" data-toggle="tab" href="#tab_3" data-swap-group="tabgroup_1" role="tab" aria-expanded="false">Number Three Tab</a>
      </li>
      <li class="__item">
        <a class="__link" data-toggle="tab" href="#tab_4" data-swap-group="tabgroup_1" role="tab" aria-expanded="false">And Tab the Fourth</a>
      </li>
    </ul>
  </nav>
</section>


<section class="section">
  <header class="k_section__header">Tabbed Nav</header>
  <div class="row">
    <div class="col">
      <nav class="nav --tabbed">
        <ul class="__list" role="tablist">
          <li class="__item">
            <a class="__link --active" data-toggle="tab" href="#tab_1" data-swap-group="tabgroup_1" data-swap-active="true" role="tab" aria-expanded="true">Tab 1</a>
          </li>
          <li class="__item">
            <a class="__link" data-toggle="tab" href="#tab_2" data-swap-group="tabgroup_1" role="tab" aria-expanded="false">Tab 2</a>
          </li>
          <li class="__item">
            <a class="__link" data-toggle="tab" href="#tab_3" data-swap-group="tabgroup_1" role="tab" aria-expanded="false">Tab 3</a>
          </li>
          <li class="__item">
            <a class="__link" data-toggle="tab" href="#tab_4" data-swap-group="tabgroup_1" role="tab" aria-expanded="false">Tab 4</a>
          </li>
        </ul>
      </nav>

      <div class="nav__content">
        <div class="hidden pt--1 --active" id="tab_1" role="tabpanel">
          <h3>Content 1</h3>
          <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum. Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div class="hidden pt--1" id="tab_2" role="tabpanel">
          <h3>Content 2</h3>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec sed odio dui. Vestibulum id ligula porta felis euismod semper. Aenean lacinia bibendum nulla sed consectetur.</p>
        </div>
        <div class="hidden pt--1" id="tab_3" role="tabpanel">
          <h3>Content 3</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
        </div>
        <div class="hidden pt--1" id="tab_4" role="tabpanel">
          <h3>Content 4</h3>
          <p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Curabitur blandit tempus porttitor. Donec id elit non mi porta gravida at eget metus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="section">
  <header class="k_section__header">Tabbed Nav: Bordered</header>
  <p class="mb--2"><i>Add <code>--bordered</code> to the <code>nav</code> element to give a line of separation from the sibling content below.</i></p>
  <div class="row">
    <div class="col">
      <nav class="nav --tabbed --bordered">
        <ul class="__list" role="tablist">
          <li class="__item">
            <a class="__link --active" data-toggle="tab" href="#tab_5" data-swap-group="tabgroup_2" role="tab" aria-expanded="true">Mozzarella Sticks</a>
          </li>
          <li class="__item">
            <a class="__link" data-toggle="tab" href="#tab_6" data-swap-group="tabgroup_2" role="tab" aria-expanded="false">Mushroom Pizza Pie</a>
          </li>
          <li class="__item">
            <a class="__link" data-toggle="tab" href="#tab_7" data-swap-group="tabgroup_2" role="tab" aria-expanded="false">Fried Ice Cream</a>
          </li>
        </ul>
      </nav>

      
      <div class="nav__content">
        <div class="hidden pt--1 --active" id="tab_5" role="tabpanel">
          <h3>Mozzarella Sticks</h3>
          <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum. Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div class="hidden pt--1" id="tab_6" role="tabpanel">
          <h3>Mushroom Pizza Pie</h3>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec sed odio dui. Vestibulum id ligula porta felis euismod semper. Aenean lacinia bibendum nulla sed consectetur.</p>
        </div>
        <div class="hidden pt--1" id="tab_7" role="tabpanel">
          <h3>Fried Ice Cream</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="section">
  <header class="k_section__header">Tabbed Nav: Buttons</header>
  <div class="row">
    <div class="col">
      <p>Button Tabs should default to no active selection, requiring engagement to reveal content, as if targeting differing content to a specific audience group.
      <nav class="nav --tabbed --buttons">
        <ul class="__list" role="tablist">
          <li class="__item">
            <a class="__link" data-toggle="tab" href="#tab_8" data-swap-group="tabgroup_3" role="tab" aria-expanded="false">Undergraduates</a>
          </li>
          <li class="__item">
            <a class="__link" data-toggle="tab" href="#tab_9" data-swap-group="tabgroup_3" role="tab" aria-expanded="false">Graduate Students</a>
          </li>
          <li class="__item">
            <a class="__link" data-toggle="tab" href="#tab_10" data-swap-group="tabgroup_3" role="tab" aria-expanded="false">Alumni</a>
          </li>
        </ul>
      </nav>
      
      <div class="nav__content">
        <div class="hidden pt--1" id="tab_8" role="tabpanel">
          <h3>Undergraduate Students</h3>
          <p>Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Maecenas faucibus mollis interdum. Sed posuere consectetur est at lobortis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <div class="hidden pt--1" id="tab_9" role="tabpanel">
          <h3>Graduate Students</h3>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec sed odio dui. Vestibulum id ligula porta felis euismod semper. Aenean lacinia bibendum nulla sed consectetur.</p>
        </div>
        <div class="hidden pt--1" id="tab_10" role="tabpanel">
          <h3>Alumni</h3>
          <p>Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ullamcorper nulla non metus auctor fringilla. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
        </div>
      </div>
      
    </div>
  </div>
</section>

<section class="section">
  <header class="k_section__header mb--2@xs">Pagination</header>
  <nav class="nav --pagination" aria-label="Page navigation">
    <ul class="__list">
      <li class="__item">
        <a class="__link" href="#" aria-label="Previous">
          <i data-feather="chevron-left"></i>
          <span class="sr--only">Previous</span>
        </a>
      </li>
      <li class="__item"><a class="__link" href="#">1</a></li>
      <li class="__item"><a class="__link" href="#">2</a></li>
      <li class="__item"><a class="__link --active" href="#">3</a></li>
      <li class="__item"><a class="__link" href="#">4</a></li>
      <li class="__item"><a class="__link" href="#">5</a></li>
      <li class="__item">
        <a class="__link" href="#" aria-label="Next">
          <i data-feather="chevron-right"></i>
          <span class="sr--only">Next</span>
        </a>
      </li>
    </ul>
  </nav>
</section>
