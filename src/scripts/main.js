// Main
//
// main entry importing dependencies

import 'jquery';

// import Formstone
import 'formstone/src/js/core';
import 'formstone/src/js/analytics';
import 'formstone/src/js/carousel';
import 'formstone/src/js/checkpoint';
import 'formstone/src/js/cookie';
import 'formstone/src/js/lightbox';
import 'formstone/src/js/swap';
import 'formstone/src/js/mediaquery';
import 'formstone/src/js/touch';
import 'formstone/src/js/transition';

// import gumshoe
import 'gumshoejs'

// import Kernl(UI) items
import './components/accordion';
import './components/carousel';
import './components/checkpoint';
import './components/gallery';
import './components/gumshoe';
import './components/icon';
import './components/loader';
import './components/masthead';
import './components/modal';
import './components/nav';
import './components/sharing';
